/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:

const personDetails = () => {
    const fullName = prompt("Full Name: ");
    const age = prompt("Age: ");
    const location = prompt("Location: ");
  
    console.log(`Hello, ${fullName}.\nYou are ${age} years old.\nYou live in ${location}.`);
  };
  
  personDetails();
  
  /*
      2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
          -invoke the function to display your information in the console.
          -follow the naming conventions for functions.
      
  */
  
  //second function here:
  
  const topFiveArtists = () => {
    console.log("Top Five Artists: ");
    console.log("1. Wala po akong favorite.");
    console.log("2. Ewan ko po");
    console.log("3. Hindi ko po alam");
    console.log("4. Wala po ako idea");
    console.log("5. Pantay po sila lahat saakin");
  };
  
  topFiveArtists();
  /*
      3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
          -Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
          -invoke the function to display your information in the console.
          -follow the naming conventions for functions.
      
  */
  
  //third function here:
  
  const topFiveMovies = () => {
    console.log("1. Kimi no Na wa Rotten\nTomatoes Rating: 98%");
    console.log("2. My Neighbor Totoro\nRotten Tomatoes Rating: 95%");
    console.log("3. My Hero Academia: Two Heroes\nRotten Tomatoes Rating: 100%");
    console.log("4. Jujutsu Kaisen 0\nRotten Tomatoes Rating: 98%");
    console.log("5. Howl's Moving Castle\nRotten Tomatoes Rating: 87%");
  };
  
  topFiveMovies();
  
  /*
      4. Debugging Practice - Debug the following codes and functions to avoid errors.
          -check the variable names
          -check the variable scope
          -check function invocation/declaration
          -comment out unusable codes.
  */
  
  printUsers();
  function printUsers() {
    alert("Hi! Please add the names of your friends.");
    let friend1 = prompt("Enter your first friend's name:");
    let friend2 = prompt("Enter your second friend's name:");
    let friend3 = prompt("Enter your third friend's name:");
  
    console.log("You are friends with:");
    console.log(friend1);
    console.log(friend2);
    console.log(friend3);
  }
  
  // console.log(friend1);
  // console.log(friend2);
  